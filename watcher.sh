#!/bin/bash

tail -fn0 /var/log/auth.log | \
while read line;
do
    LOG=$(echo $line | grep "Accepted");

    if [ $? = 0 ];
    then

        if [ ! -d $HOME/.var/log ]; then mkdir -p $HOME/.var/log; fi
        echo $LOG >> $HOME/.var/log/auth.log

        USER=$(echo $LOG | grep -oP '(?<=for ).*(?= from)');
        IP=$(echo $LOG | grep -oP '(?<=from ).*(?= port)');
        HOSTNAME=$(hostname -f);
        DATE=$(date +'%c');

        notify-send -i dialog-warning "SSH Access" "User $USER@$HOSTNAME logged in from $IP\non $DATE.";
    fi
done
